'use strict'

import React from 'react'
import PropTypes from 'prop-types'
import { Redirect, Link } from 'react-router-dom'

const Interests = ({
  user,
  handleSubmit,
  isActiveLabel,
  handleCheckboxChange,
  isSaving,
  politics,
  business,
  tech,
  science,
  sports
}) => {
  return user && !user.username
    ? <Redirect to={'/login'} />
    : <main className='app-main'>
      <form className='interests' onSubmit={handleSubmit}>
        <h1 className='interests-title'>
          welcome, <em className='title-username'>{user.username}</em>
        </h1>

        <h2 className='interests-subtitle'>my interests</h2>

        <div className='my-interests'>
          <label
            className={`interest-tag politics ${isActiveLabel('politics')}`}>
            politics
            <input
              className='input'
              name='politics'
              type='checkbox'
              checked={politics}
              onChange={handleCheckboxChange} />
          </label>

          <label
            className={`interest-tag business ${isActiveLabel('business')}`}>
            business
            <input
              className='input'
              name='business'
              type='checkbox'
              checked={business}
              onChange={handleCheckboxChange} />
          </label>

          <label
            className={`interest-tag tech ${isActiveLabel('tech')}`}>
            tech
            <input
              className='input'
              name='tech'
              type='checkbox'
              checked={tech}
              onChange={handleCheckboxChange} />
          </label>

          <label
            className={`interest-tag science ${isActiveLabel('science')}`}>
            science
            <input
              className='input'
              name='science'
              type='checkbox'
              checked={science}
              onChange={handleCheckboxChange} />
          </label>

          <label
            className={`interest-tag sports ${isActiveLabel('sports')}`}>
            sports
            <input
              className='input'
              name='sports'
              type='checkbox'
              checked={sports}
              onChange={handleCheckboxChange} />
          </label>
        </div>

        <button className='interests-submit' type='submit' disabled={isSaving}>
          {isSaving ? 'saving...' : 'save'}
        </button>

        <Link className='interests-back-link' to='/'>back to home</Link>
      </form>
    </main>
}

Interests.propTypes = {
  user: PropTypes.object,
  handleSubmit: PropTypes.func.isRequired,
  isActiveLabel: PropTypes.func.isRequired,
  handleCheckboxChange: PropTypes.func.isRequired,
  isSaving: PropTypes.bool.isRequired,
  politics: PropTypes.bool.isRequired,
  business: PropTypes.bool.isRequired,
  tech: PropTypes.bool.isRequired,
  science: PropTypes.bool.isRequired,
  sports: PropTypes.bool.isRequired
}

export default Interests
