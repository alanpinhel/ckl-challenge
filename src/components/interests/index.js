'use strict'

import React, { Component } from 'react'
import { connect } from 'react-redux'

import Interests from './interests'
import { saveInterests } from 'reducers/user/action-creators'
import { categories } from 'utils/constants'

import './interests.css'

class InterestsContainer extends Component {
  constructor () {
    super()

    this.state = {
      politics: false,
      business: false,
      tech: false,
      science: false,
      sports: false
    }

    this.handleCheckboxChange = this.handleCheckboxChange.bind(this)
    this.isActiveLabel = this.isActiveLabel.bind(this)
  }

  componentWillMount () {
    this.props.user.interests.interests
      .forEach(i => this.setState({ [i]: true }))
  }

  handleCheckboxChange (e) {
    this.setState({ [e.target.name]: e.target.checked })
  }

  isActiveLabel (category) {
    return this.state[category] ? 'active' : ''
  }

  render () {
    return <Interests
      user={this.props.user}
      handleSubmit={this.props.handleSubmit}
      isActiveLabel={this.isActiveLabel}
      handleCheckboxChange={this.handleCheckboxChange}
      isSaving={this.props.user.interests.isSaving}
      {...this.state}
    />
  }
}

const mapStateToProps = (state) => state

const mapDispatchToProps = (dispatch) => ({
  handleSubmit: e => {
    e.preventDefault()
    dispatch(saveInterests(categories.filter(c => e.target[c].checked)))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(InterestsContainer)
