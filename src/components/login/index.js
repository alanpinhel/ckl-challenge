'use strict'

import React from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

import Login from './login'
import { login } from 'reducers/user/action-creators'

import './login.css'

const LoginContainer = ({ user, handleSubmit, history }) => (
  <Login
    user={user}
    handleSubmit={handleSubmit(history)}
  />
)

const mapStateToProps = (state) => state

const mapDispatchToProps = (dispatch) => ({
  handleSubmit: history => e => {
    e.preventDefault()

    dispatch(login({
      username: e.target.username.value,
      password: e.target.password.value
    }))

    history.push('/interests')
  }
})

export default
withRouter(connect(mapStateToProps, mapDispatchToProps)(LoginContainer))
