'use strict'

import React from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'

const Login = ({ user, handleSubmit }) => {
  return user && user.username
    ? <Redirect to={'/interests'} />
    : <main className='app-main'>
      <form className='login' onSubmit={handleSubmit}>
        <h1 className='login-title'>user area</h1>

        <label className='login-label' htmlFor='username'>username</label>
        <input className='login-input' id='username' type='text' required />

        <label className='login-label' htmlFor='password'>password</label>
        <input className='login-input' id='password' type='password' required />

        <button className='login-submit' type='submit'>login</button>
      </form>
    </main>
}

Login.propTypes = {
  user: PropTypes.object,
  handleSubmit: PropTypes.func.isRequired
}

export default Login
