'use strict'

import React from 'react'
import { connect } from 'react-redux'

import Menu from './menu'
import { openMenu, closeMenu } from 'reducers/ui/action-creators'
import { categories } from 'utils/constants'

import './menu.css'

const MenuContainer = ({ ui, onOpenMenu, onCloseMenu }) => (
  <header className='app-header'>
    <Menu
      categories={categories}
      statusMenu={ui.isMenuOpened ? 'open' : ''}
      toggleMenu={ui.isMenuOpened ? onCloseMenu : onOpenMenu}
    />
  </header>
)

const mapStateToProps = (state) => state

const mapDispatchToProps = (dispatch) => ({
  onOpenMenu: () => dispatch(openMenu()),
  onCloseMenu: () => dispatch(closeMenu())
})

export default connect(mapStateToProps, mapDispatchToProps)(MenuContainer)
