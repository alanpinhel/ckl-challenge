'use strict'

import React from 'react'
import PropTypes from 'prop-types'
import { NavLink, Link } from 'react-router-dom'

const Menu = ({ categories, statusMenu, toggleMenu }) => (
  <div className='top-bar'>
    <div className={`open-menu ${statusMenu}`} onClick={toggleMenu}>
      <div className='bar1' />
      <div className='bar2' />
      <div className='bar3' />
    </div>

    <Link to='/' replace><div className='logo-challenge' /></Link>

    <nav className={`menu-app ${statusMenu}`}>
      <ul className='menu-items-list'>
        {categories.map(c => (
          <li key={c} className='item-menu'>
            <NavLink
              className='category-link'
              activeClassName='selected'
              to={`/news/${c}`}
              onClick={toggleMenu}
              replace>
              {c}
            </NavLink>
          </li>
        ))}

        <li className='item-menu'>
          <NavLink
            className='login-link'
            to='/login'
            onClick={toggleMenu}
            replace>
            login
          </NavLink>
        </li>
      </ul>
    </nav>
  </div>
)

Menu.propTypes = {
  categories: PropTypes.array.isRequired,
  statusMenu: PropTypes.string.isRequired,
  toggleMenu: PropTypes.func.isRequired
}

export default Menu
