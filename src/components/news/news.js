'use strict'

import React from 'react'
import PropTypes from 'prop-types'

import './news.css'

const News = ({ category, image, title, author, description }) => (
  <article className='news'>
    <p className={`news-category ${category}`}>{category}</p>

    <a className='news-read-more-link' href='#'>
      <section className='news-highlight'>
        {image && <p className='news-read-more'>read more</p>}

        {image && <picture>
          <source srcSet={`${image.src}, ${image.srcRetina} 2x`} />
          <img className='news-image' src={image.srcRetina} alt={image.alt} />
        </picture>}
      </section>

      <h1 className='news-title'>{title}</h1>
    </a>

    <span className='news-author'>
      <img className='author-image' src={author.image} alt={author.name} />
      <em className='author-name'>by {author.name}</em>
    </span>

    <p className='news-description'>{description}</p>
  </article>
)

News.propTypes = {
  category: PropTypes.string.isRequired,
  image: PropTypes.object,
  title: PropTypes.string.isRequired,
  author: PropTypes.object.isRequired,
  description: PropTypes.string.isRequired
}

export default News
