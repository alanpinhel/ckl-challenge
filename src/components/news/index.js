'use strict'

import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import News from './news'
import LoadingNews from './loading-news'
import { fetchNews } from 'reducers/news/action-creators'

class NewsContainer extends PureComponent {
  componentWillMount () {
    if (!this.props.news.news.length) {
      this.props.fetchNews()
    }
  }

  getNewsActive () {
    const { category } = this.props.match.params
    const { news } = this.props.news
    const { user } = this.props

    return category
      ? news.filter(n => n.category === category)
      : user.username
        ? news.filter(n => user.interests.interests.indexOf(n.category) !== -1)
        : news
  }

  getElementToRender () {
    const newsActive = this.getNewsActive()

    return this.props.news.isFetching
      ? [1, 2, 3, 4, 5, 6, 7].map(i => <LoadingNews key={i} />)
      : newsActive.length > 0
        ? newsActive.map(n => <News key={n.id} {...n} />)
        : <div className='warn-not-found'>Not found news</div>
  }

  render () {
    return (
      <main className='app-main'>
        {this.getElementToRender()}
      </main>
    )
  }
}

const mapStateToProps = (state) => ({
  news: state.news,
  user: state.user
})

const mapDispatchToProps = { fetchNews }

export default connect(mapStateToProps, mapDispatchToProps)(NewsContainer)
