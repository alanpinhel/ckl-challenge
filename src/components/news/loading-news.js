'use strict'

import React from 'react'

const LoadingNews = () => (
  <div className='news loading animated-background'>
    <div className='background-masker category-right' />
    <div className='background-masker category-bottom' />
    <div className='background-masker image-bottom' />
    <div className='background-masker title-top-right' />
    <div className='background-masker title-middle' />
    <div className='background-masker title-bottom-right' />
    <div className='background-masker title-bottom' />
    <div className='background-masker author-image-top-right' />
    <div className='background-masker author-image-middle-first' />
    <div className='background-masker author-image-middle-last' />
    <div className='background-masker author-image-bottom-right' />
    <div className='background-masker description-top' />
  </div>
)

export default LoadingNews
