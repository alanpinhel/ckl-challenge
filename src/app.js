'use strict'

import React from 'react'
import { Route, HashRouter } from 'react-router-dom'

import Menu from 'components/menu'
import News from 'components/news'
import Login from 'components/login'
import Interests from 'components/interests'

import './css/reset.css'
import './css/style.css'

const App = () => (
  <HashRouter>
    <div>
      <Menu />
      <Route exact path='/' component={News} />
      <Route path='/news/:category' component={News} />
      <Route path='/login' component={Login} />
      <Route path='/interests' component={Interests} />
    </div>
  </HashRouter>
)

export default App
