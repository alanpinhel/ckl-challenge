'use strict'

import { LOGIN, SAVING_INTERESTS, SUCCESS_INTERESTS } from './actions'

export const login = (user) => ({
  type: LOGIN,
  payload: user
})

export const saveInterests = (interests) => async (dispatch, getState) => {
  dispatch({ type: SAVING_INTERESTS })

  // Simulate time of an HTTP request
  await new Promise(resolve => setTimeout(() => resolve(), 500))

  dispatch({
    type: SUCCESS_INTERESTS,
    payload: interests
  })
}
