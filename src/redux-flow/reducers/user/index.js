'use strict'

import { LOGIN, SAVING_INTERESTS, SUCCESS_INTERESTS } from './actions'
import createReducer from '../create-reducer'

export const initialState = {
  username: null,
  password: null,
  interests: {
    interests: [],
    isSaving: false
  }
}

const news = createReducer(initialState, {
  [LOGIN]: (state, action) => ({
    ...action.payload,
    interests: state.interests
  }),

  [SAVING_INTERESTS]: (state, action) => ({
    ...state,
    interests: {
      isSaving: true
    }
  }),

  [SUCCESS_INTERESTS]: (state, action) => ({
    ...state,
    interests: {
      interests: action.payload,
      isSaving: false
    }
  })
})

export default news
