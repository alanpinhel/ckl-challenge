'use strict'

import { FETCHING, SUCCESS } from './actions'
import createReducer from '../create-reducer'

export const initialState = {
  news: [],
  isFetching: false
}

const news = createReducer(initialState, {
  [FETCHING]: (state, action) => ({
    ...state,
    isFetching: true
  }),

  [SUCCESS]: (state, action) => ({
    news: action.payload,
    isFetching: false
  })
})

export default news
