'use strict'

import ajax from '@fdaciuk/ajax'

import { FETCHING, SUCCESS } from './actions'

export const fetchNews = () => async (dispatch, getState) => {
  dispatch({ type: FETCHING })

  const response = await ajax()
    .get('https://demo9403763.mockable.io/ckl-challenge/api/news')

  setTimeout(() => {
    dispatch({
      type: SUCCESS,
      payload: response
    })
  }, 3000) // Wait time to view loading transition
}
