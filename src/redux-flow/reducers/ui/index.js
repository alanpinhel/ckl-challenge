'use strict'

import createReducer from '../create-reducer'
import { OPEN_MENU, CLOSE_MENU } from './actions'

const initialState = {
  isMenuOpened: false
}

const ui = createReducer(initialState, {
  [OPEN_MENU]: state => ({ isMenuOpened: true }),
  [CLOSE_MENU]: state => ({ isMenuOpened: false })
})

export default ui
