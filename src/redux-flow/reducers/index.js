'use strict'

import { combineReducers } from 'redux'
import news from './news'
import user from './user'
import ui from './ui'

export default combineReducers({
  news,
  user,
  ui
})
