'use strict'

const webpack = require('webpack')
const HtmlPlugin = require('html-webpack-plugin')

const common = require('./common')

module.exports = {
  devtool: 'source-map',

  entry: [
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:3000',
    'webpack/hot/only-dev-server',
    common.entry
  ],

  output: Object.assign({}, common.output, {
    publicPath: ''
  }),

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlPlugin(common.htmlPluginConfig('template.html'))
  ],

  module: {
    rules: [
      common.standardPreLoader,
      common.jsLoader,
      common.cssLoader,
      common.fileLoader
    ]
  },

  resolve: common.resolve
}
